# klimabot

## Possible datasets and APIs
- **http://openclimatedata.net/:** Pretty impressive list of all sorts of climate related data, ranging from *Global Carbon Budge*t over *EDGAR Global Emissions* Dataset
(Emissions for the three main greenhouse gases per sector and country) to *Entry Into Force of the Paris Climate Agreement*
- **https://cds.climate.copernicus.eu/api-how-to**: Dive into this wealth of information about the Earth's past, present and future climate. It is freely available and functions  as a one-stop shop to explore climate data. Register for free to obtain access to the CDS and its Toolbox.
- **http://opendata.dwd.de/**: Zurzeit stehen viele Geodaten wie Modellvorhersagen, Radardaten, aktuelle Mess- und Beobachtungsdaten sowie eine große Zahl von Klimadaten auf dem Open Data Server https://opendata.dwd.de zur Verfügung. Die Klimadaten werden unter https://opendata.dwd.de/climate_environment/CDC/ bereitgestellt.
- http://www.ipcc-data.org/
- http://www.climatus.com/index.php
- http://www.ncdc.noaa.gov/sotc/global/2012/10
- http://worldclim.org/version2
- http://www.cru.uea.ac.uk/data
- https://www.globalclimatemonitor.org/
- https://darksky.net/dev/docs

## Supported questions

### Temperatur

- Wie hoch war die Tagesdurchschnittstemperatur in **$PLACE** am **$DATE**?
- Wie hoch war die Monatsdurchschnittstemperatur in **$PLACE** im **$MONAT**?
- Wie hoch war die Jahresdurchschnittstemperatur in **$PLACE** im Jahr **$JAHR**?
- Wie groß ist die Abweichung der Durchschnittstemperatur in **$PLACE** im Vergleich zu den letzten 30 Jahren?

### Niederschlag

### Wind
- Wieviele Windräder stehen in **$ORT**?
- Wieviel Leistung liefert ein durchschnittliches Windrad?
- Wieviele Windräder stehen in **$ORT** im Jahr **$JAHR**?
- Wieviele Windräder wurden im Jahr **$JAHR** in **$ORT** gebaut?

### Solar
- Wieviel Energie liefert ein Quadratmeter Solarpanel?
- Wieviele Solarparks gibt es in **$ORT**?

### Kohle
- Wieviele Kohlekraftwerke gibt es?
- Wieviele Kohlekraftwerke gibt es in **$ORT**?

### CO2
- Wie hoch ist der CO2 Ausstoß von Menschen in **$ORT**?
- Wie hoch war der CO2 Ausstoß von Menschen in **$ORT** im **$JAHR** ?
- Wie hoch ist der CO2 Ausstoß vom Kraftwerk **$KRAFTWERK**?

## Installation

pyenv entsprechend Anleitung installieren: https://github.com/pyenv/pyenv#installation

Anschließend folgende Befehle ausführen:

```console
$ pyenv install 3.5.2
$ pyenv local 3.5.2
$ python -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ snips-nlu download de
```

## Training des Models

Zum trainieren und hinzufügen von neuen intents, muss die `yaml` Datei nach
`data/intents` gelegt werden. Der Name der Datei muss dem Namen des intents
entsprechen. Die Trainingsdaten für ein intent über `solar` muss also
`data/intents/solar.yaml` heißen. Selbes gilt für entities.

Wenn nach dem Training nun der intent `solar` erkannt wird, werden die
erkannten Informationen an die Funktion `handle` aus der `handler/solar.py`
weiter gegeben. Das Ergebnis dieser Funktion wird anschließend zurück gegeben.

Zum einsammeln der Trainingsdaten und trainieren wird einfach die `train.py`
ausgeführt. Dazu bitte sicher gehen, dass das virtual environment aktiviert ist.
Dies erkennt man daran, das `(venv)` vor dem console prompt steht. Wenn dies
nicht der Fall ist, kann das venv mittels `$ source venv/bin/activate` vom root
Verzeichnis des Projektes aus gestartet werden.

## Testen des Ergebnisses

Anschließend kann das Trainingsergebnis getestet werden mit Hilfe von
`python interprete.py "<TEXT HERE>"`. Wenn ein `intent` erkannt wurde, wird
versucht die `handle` Funktion aus der `handlers.<intent name>` zu importieren.
Wenn dies erfolgreich ist, werden die extrahierten Informationen an die `handle`
Funktion übergeben. Alternativ wird das Ergebnis von SNIPS ausgegeben.
