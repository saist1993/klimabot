import sys
import subprocess
import glob
import json
from time import gmtime, strftime

from snips_nlu import SnipsNLUEngine
from snips_nlu.default_configs import CONFIG_DE

def main():
    entities = glob.glob('data/entities/*yaml')
    intents = glob.glob('data/intents/*yaml')
    if not intents:
        print("No intent files found.")
        sys.exit()
    cmd = ['snips-nlu', 'generate-dataset', 'de', *entities, *intents]
    with open("data/dataset.json", "w") as outfile:
        subprocess.call(cmd, stdout=outfile)
    with open("data/dataset.json", "r") as infile:
        dataset = json.load(infile)
    seed = 42
    engine = SnipsNLUEngine(config=CONFIG_DE, random_state=seed)
    engine.fit(dataset)
    model_file_name = strftime("model_%Y_%m_%d_%H_%M_%S", gmtime())
    engine.persist("data/models/" + model_file_name)

if __name__ == '__main__':
    main()
